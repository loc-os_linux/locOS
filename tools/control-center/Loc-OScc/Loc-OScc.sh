#! /bin/bash

# Loc-OS Control Center
clear

HERE="$(dirname "$(readlink -f "${0}")")"
PATH="${HERE}"/usr/bin/:"${HERE}"/usr/:"${HERE}"/lib/:"${PATH}"
LD_LIBRARY_PATH="${HERE}"/usr/lib64/:"${LD_LIBRARY_PATH}"

## export PERLLIB="${HERE}"/usr/share/perl5/:"${HERE}"/usr/lib/perl5/:"${PERLLIB}"
PosDir=`find $(pwd) -maxdepth 0 -type d -not -path '*/\.*' | sort`

YAD="YadCcLocOS"
echo $YAD

CurrentDesktopLocOS=$(echo "$XDG_CURRENT_DESKTOP" | awk '{ print $1}' | tail -1)

#Language=$(cat $PosDir/.lang/Lang.info)
Language=$(echo $LANG |sed 's/[-,/,",-,_,:,¡.,[,{,},]/ /g' |awk '{ print $1}')
Language=$(echo "$Language" | tr '[:upper:]' '[:lower:]')
echo $Language

LangTitle=$(head -1 /opt/Loc-OScc/.lang/$Language | tail -1)
LangBotExit=$(head -2 /opt/Loc-OScc/.lang/$Language | tail -1)


ButtonInfo="NO"
SiNo=""
Foreground=""
#Foreground="foreground='#F0FF00'"
ForegroundBot=""
#ForegroundBot="foreground='red'"
ForegroundBotRemove="foreground='red'"
TabSizeBot="9"
FontSize="7"
FontSizeBot="7"
FontSizeCC="14"
PosDirLocOSCC=""
FieldType="BTN"

#####ID --KEY 
 id="${RANDOM}"
#id=$(echo $[($RANDOM % ($[10000 - 32000] + 1)) + 10000] )
##                


function BotExit
{
#BotExit
killall /opt/Loc-OScc/usr/bin/YadCcLocOS
#kill -USR1 $YAD_PID
exit -0
}
export -f BotExit



LangTab1=$(head -1 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab2=$(head -2 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab3=$(head -3 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab4=$(head -4 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab5=$(head -5 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab6=$(head -6 /opt/Loc-OScc/Tab_$Language.info | tail -1)
LangTab7=$(head -7 /opt/Loc-OScc/Tab_$Language.info | tail -1)
Desktop=$($YAD --plug="$id" --tabnum=1 --icons --read-dir="/opt/.CcLoc-OS/Desktop" --item-width=100) &
Software=$($YAD --plug="$id" --tabnum=2 --icons --read-dir="/opt/.CcLoc-OS/Software" --item-width=100) &
System=$($YAD --plug="$id" --tabnum=3 --icons --read-dir="/opt/.CcLoc-OS/System" --item-width=100) &
Network=$($YAD --plug="$id" --tabnum=4 --icons --read-dir="/opt/.CcLoc-OS/Network" --item-width=100) &
Hardware=$($YAD --plug="$id" --tabnum=5 --icons --read-dir="/opt/.CcLoc-OS/Hardware" --item-width=100) &
Disks=$($YAD --plug="$id" --tabnum=6 --icons --read-dir="/opt/.CcLoc-OS/Disks" --item-width=100) &
Themes=$($YAD --plug="$id" --tabnum=7 --icons --read-dir="/opt/.CcLoc-OS/Themes" --item-width=100) &

TABLCC=$($YAD --notebook --center --title="$LangTitle" --window-icon="/opt/Loc-OScc/0/02/Loc-OScc/Logo-LocOS64.png" --key="$id" \
--tab="$LangTab1"  \
--tab="$LangTab2"  \
--tab="$LangTab3"  \
--tab="$LangTab4"  \
--tab="$LangTab5"  \
--tab="$LangTab6"  \
--tab="$LangTab7"  \
--always-print-result --dialog-sep --title="Loc-OS $CurrentDesktopLocOS" --image=/opt/Loc-OScc/Logo-LocOS64.png \
     --width=760 --height=420 --center --window-icon=/opt/Loc-OScc/Logo-LocOS64.png --image-on-top --text="<span font='$FontSizeCC' $ForegroundBot><b><big>$LangTitle</big></b></span>" --button="<span font='$FontSizeBot' $ForegroundBot><b><big>$LangBotExit</big></b></span>!/opt/Loc-OScc/exit32.png!:bash -c BotExit")
ret=$?
