#!/bin/sh
cp -fR /opt/Loc-OScc/themes/Linux-XP/.config/* $HOME/.config/
cp -fR /opt/Loc-OScc/themes/Linux-XP/.icons/* $HOME/.icons/
cp -fR /opt/Loc-OScc/themes/Linux-XP/.themes/* $HOME/.themes/
pcmanfm --desktop-off
lxpanelctl restart
openbox --restart
sleep 1
pcmanfm --desktop -p LXDE &
sleep 1
sh /opt/Loc-OScc/themes/Linux-XP/fondo.sh
