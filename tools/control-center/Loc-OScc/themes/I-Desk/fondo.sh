#!/bin/bash
pcmanfm --set-wallpaper /usr/share/lxde/wallpapers/I-Desk.jpg --wallpaper-mode=crop
if ! pgrep plank; then
plank &
fi
if ! pgrep picom; then
picom -f -c -C &
fi
