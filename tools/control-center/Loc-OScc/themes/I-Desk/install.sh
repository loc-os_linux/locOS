#!/bin/bash
cp -fR /opt/Loc-OScc/themes/I-Desk/.config/* $HOME/.config
cp -fR /opt/Loc-OScc/themes/I-Desk/.local/* $HOME/.local
cp -fR /opt/Loc-OScc/themes/I-Desk/.themes/* $HOME/.themes
cp -fR /opt/Loc-OScc/themes/I-Desk/.icons/* $HOME/.icons
cp -f /opt/Loc-OScc/themes/I-Desk/.config/openbox/lxde-rc.xml $HOME/.config/openbox
pcmanfm --desktop-off
openbox --restart
lxpanelctl restart
sleep 1
pcmanfm --desktop -p LXDE &
sleep 1
sh /opt/Loc-OScc/themes/I-Desk/fondo.sh
