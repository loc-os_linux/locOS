#!/bin/bash
cp -fR /opt/Loc-OScc/themes/LXDE-Neon/.config/* $HOME/.config
cp -fR /opt/Loc-OScc/themes//LXDE-Neon/.local/* $HOME/.local
cp -fR /opt/Loc-OScc/themes/LXDE-Neon/.themes/* $HOME/.themes
cp -fR /opt/Loc-OScc/themes/LXDE-Neon/.icons/* $HOME/.icons
cp -f /opt/Loc-OScc/themes/LXDE-Neon/.config/openbox/lxde-rc.xml $HOME/.config/openbox
pcmanfm --desktop-off
openbox --restart
lxpanelctl restart
sleep 1
pcmanfm --desktop -p LXDE &
sleep 1
sh /opt/Loc-OScc/themes/LXDE-Neon/fondo.sh
