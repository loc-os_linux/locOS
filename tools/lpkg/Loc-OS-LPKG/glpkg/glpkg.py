#!/bin/python3
import time
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk, GObject

from glpkg_lib import *
xpkgdesc(sys.argv)
time.sleep(0.2)
import pkgdesc
dp=": "
# Ventana que se usa si hay otra instancia abierta
class Ocupado(Gtk.Window):
    def __init__(self):
        super().__init__(title="Glpkg")
        self.error = Gtk.Label(label="\n"+socupado+"\n")
        self.add(self.error)

# Ventana para recibir la contraseña de sudo
class Sudo(Gtk.Window):
    def __init__(self):
        super().__init__(title="sudo")
        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.label = Gtk.Label(label=spassword+dp)
        self.box.pack_start(self.label, True, True, 0)
        self.entry = Gtk.Entry()
        self.entry.set_visibility(False)
        self.entry.connect("activate",self.save,self.entry)
        self.box.pack_start(self.entry, True, True, 0)
        self.add(self.box)
        self.passw=""
    def save(self,button,entry):
        self.passw = self.entry.get_text()
        subprocess.run(["/opt/Loc-OS-LPKG/glpkg/glpkg-lib.sh","writetmp",self.passw])
        self.destroy()

# Ventana principal
class Ventana(Gtk.Window):
    def __init__(self):
            super().__init__(title="Glpkg")
            
            # Definiendo cajas y variables principales
            self.lpkg = str(xname(lpkg[1]))
            self.set_resizable(False)
            # self.spinner = Gtk.Spinner()
            self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.hbox = Gtk.Box(spacing=10)
            self.hbox.set_homogeneous(False)
            self.vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.vbox_center = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.dhbox = Gtk.Box(spacing=10)
            self.nbox = Gtk.Box()
            self.nbox1 = Gtk.Box()
            self.nbox2 = Gtk.Box()

            # Primera caja
            self.hbox.pack_start(self.vbox_left, True, True, 0)
            self.hbox.pack_start(self.vbox_center, True, True, 0)
            self.hbox.pack_start(self.nbox1, True, True, 0)
            self.hbox.pack_start(self.vbox_right, True, True, 0)
            self.hbox.pack_start(self.nbox2, True, True, 0)

            # Icono
            self.icon = Gtk.Image()
            self.icon.set_from_file(r"/opt/Loc-OS-LPKG/glpkg/icon-lpkg.png")
            self.vbox_left.pack_start(self.icon, True, True, 0)
    
            # Paquete
            self.lpaquete = Gtk.Label(label=spaquete+dp+pkgdesc.paquete)
            self.lpaquete.set_justify(Gtk.Justification.LEFT)
            self.vbox_center.pack_start(self.lpaquete, True, True, 0)
            
            # Version
            self.lversion = Gtk.Label(label=sversion+dp+pkgdesc.version)
            self.lversion.set_justify(Gtk.Justification.LEFT)
            self.vbox_center.pack_start(self.lversion, True, True, 0)

            # Estado
            self.lstatus = Gtk.Label(label=sstatus+dp+self.verificar("",self.lpkg))#"Instalado")
            self.lstatus.set_justify(Gtk.Justification.LEFT)
            self.vbox_center.pack_start(self.lstatus, True, True, 0)

            #Botones y etiquetas de instalción
            self.binstalar = Gtk.Button(label="")
            self.binstalar.connect("clicked", self.instalar)
            self.binstalar.hide()

            self.ltrabajando = Gtk.Label(label="")
            self.ltrabajando.hide()

            self.bdesinstalar = Gtk.Button(label="")
            self.bdesinstalar.connect("clicked", self.desinstalar)
            self.bdesinstalar.hide()

            self.null = Gtk.Label(label="")
            self.null.hide()

            self.refrescar()

            # Frame "Detalles"
            self.fbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=2)
            self.lmantenedor = Gtk.Label(label=smantenedor+dp+pkgdesc.mantenedor)
            self.llicencia = Gtk.Label(label=slicencia+dp+pkgdesc.licencia)
            self.lhomepage = Gtk.Label()
            self.lhomepage.set_markup("  "+shomepage+dp+"<a href=\""+pkgdesc.homepage+"\"\
                 title=\""+sirweb+"\">"+pkgdesc.homepage+"</a>  ")
            self.indexlink = "https://loc-os_linux.gitlab.io/lpkgindex/pages/"+pkgdesc.paquete#+".html"
            self.lindex = Gtk.Label()
            self.lindex.set_markup("  "+"Index"+dp+"<a href=\""+self.indexlink+"\"\
                 title=\""+sirindex+"\">"+self.indexlink+"</a>  ")
            self.desc = Gtk.Frame(label=" "+sdetalles)
            self.dhbox.pack_start(self.desc, True, True, 0)
            self.desc.add(self.fbox)
            self.fbox.pack_start(self.lmantenedor, True, True, 0)
            self.fbox.pack_start(self.llicencia, True, True, 0)
            self.fbox.pack_start(self.lhomepage, True, True, 0)
            if verindex(self.indexlink) == True:
                self.fbox.pack_start(self.lindex, True, True, 0)

            # Agregar cajas
            self.add(self.box)
            self.box.pack_start(self.hbox, True, True, 0)
            self.box.pack_start(self.dhbox, True, True, 0)
    
    # Verifica estado de instalación
    def verificar(self,button,lpkg):
        iestado = verificar(lpkg)
        return iestado
    
    # Actualiza los botones según el estado de instalación
    def refrescar(self):
        elements = [self.ltrabajando,self.null,self.bdesinstalar,self.binstalar]
        for label in elements:
            if label.props.label != "":
                label.hide()
        time.sleep(1)
        if snoinstalado in self.lstatus.get_text():
            
            if self.binstalar.props.label == "":
                self.vbox_right.pack_start(self.binstalar, True, True, 0)
            else:
                self.binstalar.show()
            self.binstalar.set_label(sinstalar)

            
            if self.null.props.label == "":
                self.vbox_right.pack_start(self.null, True, True, 0)
                self.null.set_text("\n")
                self.null.show()
            else:
                self.null.show()
            self.null.set_text("\n")
            

        elif sinstalado in self.lstatus.get_text():

            if self.binstalar.props.label == "":
                self.vbox_right.pack_start(self.binstalar, True, True, 0)
            else:
                self.binstalar.show()
            self.binstalar.set_label(sreinstalar)
            
            if self.bdesinstalar.props.label == "":
                self.vbox_right.pack_start(self.bdesinstalar, True, True, 0)
                self.bdesinstalar.set_label(sdesinstalar)
                self.bdesinstalar.show()
            else:
                self.bdesinstalar.show()
            
    # Instalar/Reinstalar el paquete
    def instalar(self,button):
        self.ltrabajando.set_text(sinstalando)
        self.ltrabajando.show()

        #sudo = Sudo()
        #sudo.connect("destroy", Gtk.main_quit)
        #sudo.show_all()
        #Gtk.main()

        instalar(lpkg)
        self.lstatus.set_text(sstatus+dp+self.verificar("",self.lpkg))
        self.refrescar()
        return sstatus
    
    # Desinstalar el paquete
    def desinstalar(self,button):
        self.ltrabajando.set_text(sdesinstalando)
        self.ltrabajando.show()

        #sudo = Sudo()
        #sudo.connect("destroy", Gtk.main_quit)
        #sudo.show_all()
        #Gtk.main()

        remover(self.lpkg)
        self.lstatus.set_text(sstatus+dp+self.verificar("",self.lpkg))
        self.refrescar()


# Si el programa se detuvo antes de tiempo
# Ejecute el comando "rm /tmp/glpkg.lock" 
# o reinicie la computadora para eliminar el bloqueo
def quit(self): # Salida segura
    Gtk.main_quit(self)
    subprocess.Popen(["rm","/tmp/glpkg.lock"])
    subprocess.Popen(["rm","-f","/tmp/pglpkg.tmp"])

# Inicio del programa
if "glpkg.lock" not in subprocess.getoutput("ls /tmp/"):
    subprocess.Popen(["touch", "/tmp/glpkg.lock"])
    
    #sudo = Sudo()
    #sudo.connect("destroy", Gtk.main_quit)
    #sudo.show_all()
    #Gtk.main()
    
    start = Ventana()
    start.connect("destroy", quit)
    start.show_all()
    Gtk.main()
# Si está ocupado
else:
    start = Ocupado()
    start.connect("destroy", Gtk.main_quit)
    start.show_all()
    Gtk.main()
