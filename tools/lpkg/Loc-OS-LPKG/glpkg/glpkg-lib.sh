#!/bin/bash
case $1 in
    xpkgdesc)
        tar -C /tmp/ -xvf $2 ./desc/pkgdesc
        #cat /tmp/pglpkg.tmp | sudo -S cp -f /tmp/desc/pkgdesc /opt/glpkg/pkgdesc.py
        cp -f /tmp/desc/pkgdesc /opt/Loc-OS-LPKG/glpkg/pkgdesc.py
        rm -rf /tmp/desc ;;
    install)
        #cat /tmp/pglpkg.tmp | sudo -S lpkg install $2 ;;
        lpkg install $2 ;;
    remove)
        ##cat /tmp/pglpkg.tmp | sudo -S lpkg remove $2 ;;
        lpkg remove $2 ;;
    list)
        /usr/sbin/lpkg -i | grep $2 ;;
    writetmp)
        echo $2 > /tmp/pglpkg.tmp ;;
    *)
        echo "Error: glpkg-lib" ;;
esac
