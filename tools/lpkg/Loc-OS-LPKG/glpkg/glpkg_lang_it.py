#!/bin/python3
# Strings for translate
# Italian Translate by ItzSelenux

# Buttons
sinstalar="Installare"
sreinstalar="Reinstallare"
sdesinstalar="Disinstallare"

# Labels
spaquete="Pacchetto"
sversion="Versione"
smantenedor="Manutentore"
slicencia="Licenza"
shomepage="Sito Web"
sstatus="Stato"
sdetalles="Dettagli"
sirweb="Vai al sito web"
sirindex="Vai alla pagina web di lpkg index"
spassword="Immettere la tua password"

# Statuses
sinstalado="Installed"
snoinstalado="Non installato"
sinstalando="Installando"
sdesinstalando="Disinstallando"
socupado="Gpkg è già occupato, per favore chiudi prima le altre istanze"
