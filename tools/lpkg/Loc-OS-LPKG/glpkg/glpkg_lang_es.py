#!/bin/python3
# Strings para traducir
# Spanish Translate by Anonyzard

# Botones
sinstalar="Instalar"
sreinstalar="Reinstalar"
sdesinstalar="Desinstalar"

# Etiquetas
spaquete="Paquete"
sversion="Versión"
smantenedor="Mantenedor"
slicencia="Licencia"
shomepage="Sitio web"
sstatus="Estado"
sdetalles="Detalles"
shomepage="Página web"
sirweb="Ir a la página web"
sirindex="Ir a la página web del lpkg index"
spassword="Ingrese su contraseña"

# Estados
sinstalado="Instalado"
snoinstalado="No instalado"
sinstalando="Instalando"
sdesinstalando="Desinstalando"
socupado="Glpkg ya está ocupado, ciere la otra instancia primero"