#!/bin/python3
import subprocess
import os
import sys
from glpkg_lang import *

lpkg=sys.argv

# Extraer nombre del paquete
def xname(arg):
    global name
    print(arg)
    name=""
    name1=""
    name1=[]
    name2=[]
    count=0
    for char in str(arg):
        if count < len(arg)-5:
            name1.append(str(char))
            count+=1
        else:
            break
    name1 = list(name1)
    name1.reverse()
    for char in name1:
        if char != "/":
            name2.insert(0,char)
        else:
            break
    for n in name2:
        name+=n
    return name

# Extraer y leer pkgdesc
def xpkgdesc(arg):
    global lpkg
    try:
        arg=arg[1]
        subprocess.run(["/opt/Loc-OS-LPKG/glpkg/glpkg-lib.sh","xpkgdesc",arg])
        lpkg = arg
    except IndexError:
        print("IndexError: There isn't any lpkg")

# Definiendo función de instalación
# solo se podrá instalar un paquete a la vez
def instalar(lpkg):
    if len(lpkg) == 2:
        lpkg = lpkg[1]
    #if lpkg[0] != "/":
    #    pwd = subprocess.run(["echo","$PWD"])
    #    lpkg = pwd+"/"+lpkg
    print(lpkg)
    subprocess.run(["/opt/Loc-OS-LPKG/glpkg/glpkg-lib.sh","install",lpkg])

# Verifica si está instalado
def verificar(name):
    instalado = subprocess.getoutput("/opt/Loc-OS-LPKG/glpkg/glpkg-lib.sh list "+name)
    print(instalado, name)
    if name in instalado:
        print("Está instalado")
        return sinstalado
    elif name not in instalado:
        print("No está instalado")
        return snoinstalado
    else:
        print("Error al verificar")

# Función de desinstalación
def remover(lpkg):
    if len(lpkg) == 2:
        lpkg = lpkg[1]
    print("Removiendo paquete")
    print(subprocess.getoutput(["echo $PWD"]))
    subprocess.run(["/opt/Loc-OS-LPKG/glpkg/glpkg-lib.sh","remove",lpkg])

# Verifica que exista la página del lpkgindex
def verindex(link):
    status = subprocess.getoutput('curl --write-out "%{http_code}\n" --silent --output /dev/null "'+link+'"')
    if status == "200":
        return True
    else:
        return False
