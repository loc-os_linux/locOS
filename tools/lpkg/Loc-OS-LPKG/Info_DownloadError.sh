#! /bin/bash
clear
cd /opt/Loc-OS-LPKG/
HERE="$(dirname "$(readlink -f "${0}")")"
PATH="${HERE}"/usr/bin/:"${HERE}"/usr/:"${HERE}"/lib/:"${PATH}"
LD_LIBRARY_PATH="${HERE}"/usr/lib64/:"${LD_LIBRARY_PATH}"

## export PERLLIB="${HERE}"/usr/share/perl5/:"${HERE}"/usr/lib/perl5/:"${PERLLIB}"

YAD="YadDownload"

Language=$(echo $LANG |sed 's/[-,/,",-,_,:,¡.,[,{,},]/ /g' |awk '{ print $1}')
Language=$(echo "$Language" | tr '[:upper:]' '[:lower:]')
echo $Language

NamePackage=$(cat /tmp/.LPKG/InstallLpkg.info |grep "NameLpkg<->" |awk '{ print $2}')
LangNamePackage1=$(head -27 /opt/Loc-OS-LPKG/.lang/$Language | tail -1)
Package=$(head -28 /opt/Loc-OS-LPKG/.lang/$Language | tail -1)
PackageNot=$(head -29 /opt/Loc-OS-LPKG/.lang/$Language | tail -1)
Failed=$(head -30 /opt/Loc-OS-LPKG/.lang/$Language | tail -1)
LangNamePackage2=$(echo $LangNamePackage1 |sed 's/<_-_>name_package<_-_>/'$NamePackage'/')
LangBotExit=$(head -5 $PosDir/.lang/$Language | tail -1)
echo $LangNamePackage2

$YAD  --title="" --undecorated --center --skip-taskbar --borders="15" \
     --image=/opt/Loc-OS-LPKG/usr/.icon/close48.png \
     --height=60 \
     --button="$LangBotExit!/opt/Loc-OS-LPKG/usr/.icon/exit32.png!":1 \
     --center \
     --text-align=center \
     --text='<span font="18">'" $LangNamePackage2
$Package $NamePackage
$PackageNot
$Failed"'</span>' 

exit -0

