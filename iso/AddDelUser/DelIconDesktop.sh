#! /bin/bash
clear

XDGDESKTOPDIR=$(cat $HOME/.config/user-dirs.dirs | grep "XDG_DESKTOP_DIR=")
DESKTOP=$(echo $XDGDESKTOPDIR |sed 's/[=,",/]/ /g' |awk '{ print $3}')

NameUser=$(cat /opt/AddDelUser/.live/UserLive.info)
PasswordRoot=$(cat /opt/AddDelUser/.live/AddDataInfo.info | grep "PasswordRoot >" | awk '{ print $3}')
PasswordUser=$(cat /opt/AddDelUser/.live/AddDataInfo.info | grep "PasswordUser >" | awk '{ print $3}')

rm -f --force $HOME/$DESKTOP/*.desktop
rm -f --force $HOME/.config/autostart/DelInstallerLive.desktop
rm -f --force $HOME/$DESKTOP/Leeme.txt
rm -f --force $HOME/$DESKTOP/*.txt

rm -Rf --force $HOME/.fluxbox
rm -Rf --force $HOME/.icewm
rm -Rf --force $HOME/.jwm

#echo "NameUser -> $NameUser"
#echo "Password User -> $PasswordUser"
#echo "Password Root -> $PasswordRoot"

rm -f --force /tmp/CommandSudo.sh

echo '
rm -Rf --force /home/'$NameUser'
rm -Rf --force /etc/skel/.fluxbox
rm -Rf --force /etc/skel/.icewm
rm -Rf --force /etc/skel/.jwm
rm -f --force /tmp/CommandSudo.sh' > /tmp/CommandSudo.sh

PasswordRoot=$(echo $PasswordUser | sudo -S sh /tmp/CommandSudo.sh)





