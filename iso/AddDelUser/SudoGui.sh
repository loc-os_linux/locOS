#! /bin/bash


#who=`who` 
#Who="UserWho $who"  
#Who=$(echo $HOME |sed 's/[/]/ /g' |awk '{ print $2}')
#rm -f --force > /tmp/WhoUSER
#echo $Who > /tmp/WhoUSER
#WhoUser=$(cat /tmp/.GKWhoUSER/USER.info)


HERE="$(dirname "$(readlink -f "${0}")")"
PATH="${HERE}"/usr/bin/:"${HERE}"/usr/:"${HERE}"/:"${PATH}"
LD_LIBRARY_PATH="${HERE}"/usr/lib64/:"${LD_LIBRARY_PATH}"

## export PERLLIB="${HERE}"/usr/share/perl5/:"${HERE}"/usr/lib/perl5/:"${PERLLIB}"

cd $HERE
#GKPACKAGEPREFIX=/tmp/GkSudo
#ls ${GKPACKAGEPREFIX} || mkdir -p ${GKPACKAGEPREFIX}
PosDir="$HERE"

#echo "$1" > /tmp/GkSudo/Command.info
#Get AppImage
#echo "$1" > $GKPACKAGEPREFIX/InstallGkPackage.info
# PosDirAppImage=$(cat InstallGkPackage.info)
#if [ "$1" = "" ]; then
#echo "/tmp/GkSudo" > $GKPACKAGEPREFIX/InstallGkPackage.info
#fi

Language=""
Language=$(cat $HOME/.GkAppImageLocal/Lang.info)
if [ "$Language" = "" ]; then
Language="en"
fi

LangTitle=$(head -1 $PosDir/.lang/$Language | tail -1)
LangInstaller=$(head -2 $PosDir/.lang/$Language | tail -1)
LangInsertPassword1=$(head -3 $PosDir/.lang/$Language | tail -1)
LangInsertPassword2=$(head -4 $PosDir/.lang/$Language | tail -1)
LangBotExit=$(head -6  $PosDir/.lang/$Language | tail -1)
LangTitleError=$(head -7  $PosDir/.lang/$Language | tail -1)
LangPasswordIncorrect=$(head -8  $PosDir/.lang/$Language | tail -1)
IndentAgainPassword=$(head -9  $PosDir/.lang/$Language | tail -1)

ForegroundBot=""
#ForegroundBot="foreground='red'"
FontSizeBot="13"
FontSize="13"

PassWord=""
YAD="yadSudo"

UserLiveInfo=$(cat /opt/AddDelUser/.live/UserLive.info)
StartAddNewUser=""
NotUserLiveInfo=""
StartAddNewUser=$(cat /tmp/.StartAddNewUser)
if [ "$StartAddNewUser" = "Start AddNewUser Not Live User" ]; then
NotUserLiveInfo=$(echo $HOME |sed 's/[/]/ /g' |awk '{ print $2}')
UserLiveInfo="$NotUserLiveInfo"
fi

LiveUser=$(echo "$UserLiveInfo" | tr '[:lower:]' '[:upper:]')
LangPasswordIncorrect=$(head -8  $PosDir/.lang/$Language | tail -1 |sed 's/<_USER_>/'$LiveUser'/g')

echo $YAD
cd /tmp/GkSudo
rm -f --force Button.info
# --text='<span font="16" weight="Bold">'" $LangInstaller "'</span>' \
Password_Root=$($YAD --title="$LangTitle" --picture --size=orig \
--window-icon="$PosDir/UserRoot64.png" \
--image="$PosDir/UserRoot64.png" \
--form --separator="<><>" \
--field="<span font='$FontSizeBot' $ForegroundBot><b>$LangInsertPassword1</b></span>:LBL" "" \
--field="<span font='$FontSizeBot' $ForegroundBot><b>$LangInsertPassword2 $LiveUser</b></span>":H "" \
--field="<span font='$FontSizeBot' $ForegroundBot><b><big>$LangBotExit</big></b></span>!/opt/AddRemove_AppsPortable/usr/.icon/exit24.png!":FBTN 'bash -c "echo "EXIT" > Button.info ; kill -USR1 $YAD_PID"'  \
--no-buttons --center --width=600 --height=110 --fixed)

ButtonInfo=`cat Button.info`
if [ "$ButtonInfo" = "EXIT" ]; then
exit -0
fi

PasswordRoot="$Password_Root"
PasswordRoot=$(echo $Password_Root |sed 's/[<,>]//g' |awk '{ print $1}')
echo "$PasswordRoot" > /tmp/GkSudo/PassWord.temp

rm -f --force /tmp/GkSudo/TestPassword.info
echo $PasswordRoot | sudo -S -k echo "PassWord ok" > /tmp/GkSudo/TestPassword.info


Password=$(cat /tmp/GkSudo/TestPassword.info)

if [ "$Password" = "" ]; then
echo $PasswordRoot
echo "error PassWord Root"
Password=""
 
SudoRootError=$($YAD --title="$LangTitleError" --picture --size=orig \
--window-icon="$PosDir/PassWordError128.png" \
--image="$PosDir/PassWordError128.png" \
--text='<span font="13" weight="Bold">'" $LangPasswordIncorrect "'</span>' \
--form --separator="<><>" \
--field="<span font='$FontSizeBot' $ForegroundBot><b><big>$IndentAgainPassword</big></b></span>":FBTN 'bash -c "echo "SudoGuiSh" > Button.info ; kill -USR1 $YAD_PID"' \
--field="<span font='$FontSizeBot' $ForegroundBot><b><big>$LangBotExit</big></b></span>":FBTN 'bash -c "echo "EXIT" > Button.info ; kill -USR1 $YAD_PID"'  \
--no-buttons --center --width=600 --fixed)
fi


ButtonInfo=`cat Button.info`
if [ "$ButtonInfo" = "EXIT" ]; then
exit -0
fi

if [ "$ButtonInfo" = "SudoGuiSh" ]; then
sh $HERE/SudoGui.sh
exit -0
fi

if [ "$Password" = "PassWord ok" ]; then
PasswordRoot=$(cat /tmp/GkSudo/PassWord.temp)
rm -f --force /tmp/GkSudo/PassWord.temp
PasswordRoot=$(echo $PasswordRoot | sudo -S /tmp/GkSudo/CommandSudo.sh)
fi

exit -0
