#! /bin/bash

# GPL

NameUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "NameUser >" | awk '{ print $3}')
PasswordUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "PasswordUser >" | awk '{ print $3}')
PasswordRoot=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "PasswordRoot >" | awk '{ print $3}')
DelUserLive=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "DelUserLive >" | awk '{ print $3}')
CreateNewUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "CreateNewUser >" | awk '{ print $3}')
DelUserLive=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "DelUserLive >" | awk '{ print $3}')
CreateNewUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "CreateNewUser >" | awk '{ print $3}')

AutologinUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep "AutologinUser >" | awk '{ print $6}')
#WhoUser=$(cat /tmp/.LiveWhoUSER/USER.info)
UserLiveInfo=$(cat /opt/AddDelUser/.live/UserLive.info)


UserDel="$UserLiveInfo"
MvUser="$NameUser"

DelLiveUserDvd="$UserLiveInfo"
AddUser="$NameUser"
password="$PasswordUser"

cd /tmp/.Install_LocOs
 
if [ "$CreateNewUser" = "FALSE" ]; then
DelUserLive="FALSE"
fi
if [ "$AutologinUser" = "FALSE" ]; then
AUTOLOGIN="false"
fi
if [ "$AutologinUser" = "TRUE" ]; then
AUTOLOGIN="true"
fi

mkdir -p /etc/skel/.config/autostart
rm -f --force /etc/skel/.config/autostart/lxrandr-autostart.desktop
cp -f --force /opt/AddDelUser/.live/lxrandr-autostart.desktop /etc/skel/.config/autostart/lxrandr-autostart.desktop
cp -f --force /opt/AddDelUser/DelInstallerLive.desktop /etc/skel/.config/autostart/DelInstallerLive.desktop


rm -f --force /opt/AddDelUser/.live/AddDataInfo.info
cp -f --force /tmp/.Install_LocOs/AddDataInfo.info /opt/AddDelUser/.live/AddDataInfo.info


if [ "$CreateNewUser" = "TRUE" ]; then
#
rm -f --force /etc/slimski.local.conf
cp --f --force /tmp/.Install_LocOs/slimski.local.conf /etc/slimski.local.conf
#
DEFAULTGROUPS="adm,root,sudo,audio,cdrom,dialout,floppy,video,plugdev,netdev,disk"
useradd -g 0 -c "$AddUser" -G $DEFAULTGROUPS -s /bin/bash -m $AddUser
echo $AddUser:$PasswordUser | chpasswd
echo root:$PasswordRoot | chpasswd
fi

rm -f --force /etc/skel/.config/autostart/DelInstallerLive.desktop

rm -f --force /etc/skel/Desktop/*.desktop

if [ "$DelUserLive" = "TRUE" ]; then
cp -f --force /etc/passwd passwd.temp
cp -f --force /etc/shadow shadow.temp
cp -f --force /etc/group group.temp
cp -f --force /etc/gshadow gshadow.temp
DelUserSiNo="SI"
if [ "$DelUserSiNo" = "SI" ]; then
sed '/'$UserDel'/d' passwd.temp > passwd.temp2
sleep 1
sed '/'$UserDel'/d' shadow.temp > shadow.temp2
sleep 1
# 
rm -f --force VerUser.temp1
rm -f --force VerUser.temp2
DelUserGroupSiNo="NO"
cat group.temp |grep "$UserDel" |awk '{ print $1}' > VerUser.temp1
cat VerUser.temp1 |grep "$UserDel" |sed 's/[:]/  /g' > VerUser.temp2
VerUser=`cat VerUser.temp2 |grep "$UserDell" |awk '{ print $1}'`
if [ "$VerUser" = "$UserDel" ]; then
DelUserGroupSiNo="SI"
fi
if [ "$DelUserGroupSiNo" = "SI" ]; then
echo "Deluser $VerUser ---> Group"
sed '/'$UserDel'/d' group.temp > group.temp2
rm -f --force group.temp
mv -iv group.temp2 group.temp
fi
# 
rm -f --force VerUser.temp1
rm -f --force VerUser.temp2
DelUserGroupSiNo="NO"
cat gshadow.temp |grep "$UserDel" |awk '{ print $1}' > VerUser.temp1
cat VerUser.temp1 |grep "$UserDel" |sed 's/[:]/  /g' > VerUser.temp2
VerUser=`cat VerUser.temp2 |grep "$UserDell" |awk '{ print $1}'`
if [ "$VerUser" = "$UserDel" ]; then
DelUserGroupSiNo="SI"
fi
if [ "$DelUserGroupSiNo" = "SI" ]; then
echo "Deluser $VerUser ---> Group"
sed '/'$UserDel'/d' gshadow.temp > gshadow.temp2
rm -f --force gshadow.temp
mv -iv gshadow.temp2 gshadow.temp
fi
# 
rm -f --force passwd.temp
rm -f --force shadow.temp
sleep 1
mv -iv passwd.temp2 passwd.temp
mv -iv shadow.temp2 shadow.temp
fi
DelUserSiNo="SI"
if [ "$DelUserSiNo" = "SI" ]; then
cat group.temp |sed 's/'$UserDel'/_-DelUser-_/g' > group.temp2
rm -f --force group.temp
mv -iv group.temp2 group.temp
fi
# 
sleep 1
cat group.temp |sed 's/,_-DelUser-_//g' > group.temp2
rm -f --force group.temp
mv -iv group.temp2 group.temp
sleep 1
cat group.temp |sed 's/_-DelUser-_//g' > group.temp2
rm -f --force group.temp
mv -iv group.temp2 group.temp
DelUserSiNo="SI"
if [ "$DelUserSiNo" = "SI" ]; then
cat gshadow.temp |sed 's/'$UserDel'/_-DelUser-_/g' > gshadow.temp2
rm -f --force gshadow.temp
mv -iv gshadow.temp2 gshadow.temp
fi
# 
sleep 1
cat gshadow.temp |sed 's/,_-DelUser-_//g' > gshadow.temp2
rm -f --force gshadow.temp
mv -iv gshadow.temp2 gshadow.temp
sleep 1
cat gshadow.temp |sed 's/_-DelUser-_//g' > gshadow.temp2
rm -f --force gshadow.temp
mv -iv gshadow.temp2 gshadow.temp
fi



exit -0

