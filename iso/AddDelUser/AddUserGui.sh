#! /bin/bash

# GPL
clear


 

HERE="$(dirname "$(readlink -f "${0}")")"
PATH="${HERE}"/usr/bin/:"${HERE}"/usr/:"${HERE}"/:"${PATH}"
LD_LIBRARY_PATH="${HERE}"/usr/lib64/:"${LD_LIBRARY_PATH}"

## export PERLLIB="${HERE}"/usr/share/perl5/:"${HERE}"/usr/lib/perl5/:"${PERLLIB}"
PosDir=`find $(pwd) -maxdepth 0 -type d -not -path '*/\.*' | sort`
UserLiveInfo=$(cat /opt/AddDelUser/.live/UserLive.info)
DelLiveUser=$(echo "$UserLiveInfo" | tr '[:lower:]' '[:upper:]')
YAD="YadAddUser"

Title="Crear usuario e instalar Loc-OS"
Txt0="Crear un nuevo usuario local al instalar Loc-OS"
Txt1="Nombre de usuario"
Txt2="Contraseña para el usuario"
Txt3="Contraseña para la cuenta de administrador"
Txt4="Eliminar usuario > $DelLiveUser LIVE"
Txt5="Crear usuario"
Txt6="Instalar"
Txt7="Salir"
Txt8="Ayuda"

TextAutologin="Autologin"
NoText=""
ForegroundBot=""
#ForegroundBot="foreground='red'"
ForegroundBotRemove="foreground='red'"
TabSizeBot="12"
if [ "$Language" = "de" ]; then
TabSizeBot="10"
fi
FontSize="14"
FontSizeBot="14"
PosDirAppImage=""
FieldType="BTN"

Type="<span font='$FontSize' $ForegroundBot><b><big>$Txt1</big></b></span>"

AddUser=$($YAD --form --undecorated --skip-taskbar --center --borders="15" --fixed  \
    --separator="<InstallLive>" \
    --title="$Title" \
    --image="/opt/AddDelUser/usr/icons/Logo-LocOs-Install.png" --image-on-top \
    --field "<span font='$FontSize' $ForegroundBot><b>$Txt0</b></span>":lbl "" \
    --window-icon="$HERE/usr/icons/AddUser64.png" \
    --field "$Txt1" '' \
    --field "$Txt2":H '' \
    --field "$Txt3":H '' \
    --field "$Txt4:CHK" FALSE \
    --field "$Txt5:CHK" FALSE \
    --field "$TextAutologin:CHK" TRUE \
--button="<span font='$TabSizeBot' $ForegroundBot><b><big>$Txt6</big></b></span>!${HERE}/usr/icons/Dialog-apply48.png!":2 \
--button="<span font='$TabSizeBot' $ForegroundBot><b><big>$Txt7</big></b></span>!${HERE}/usr/icons/exit24.png!":3)
ret=$?

FALSE="FLASE"
AddDataInfo=$(echo $AddUser | grep  "Live><Install" | sed 's/Live><Install/Live>FLASE<Install/g')
if [ "$AddDataInfo" = "" ]; then
echo ok
FALSE=""
AddDataInfo="$AddUser"
fi
AddDataInfo=$(echo $AddDataInfo | grep  "<InstallLive>" |sed 's/<InstallLive>/ /g')
AddDataInfo="$FALSE $AddDataInfo"

NameUser=$(echo $AddDataInfo | awk '{ print $1}')
PasswordUser=$(echo $AddDataInfo | awk '{ print $2}')
PasswordRoot=$(echo $AddDataInfo | awk '{ print $3}')
DelUserLive=$(echo $AddDataInfo | awk '{ print $4}')
CreateNewUser=$(echo $AddDataInfo | awk '{ print $5}')
AutologinUser=$(echo $AddDataInfo | awk '{ print $6}')

WhoUser=$(echo $HOME |sed 's/[/]/ /g' |awk '{ print $2}')

if [ "$CreateNewUser" = "FALSE" ]; then
DelUserLive="FALSE"
PasswordUser="FALSE"
PasswordRoot="FALSE"
NameUser="$WhoUser"
fi

if [ "$AutologinUser" = "FALSE" ]; then
AUTOLOGIN="false"
fi
if [ "$AutologinUser" = "TRUE" ]; then
AUTOLOGIN="true"
fi

echo "
NameUser > $NameUser
PasswordUser > $PasswordUser
PasswordRoot > $PasswordRoot
DelUserLive > $DelUserLive
CreateNewUser > $CreateNewUser
AutologinUser > $AutologinUser" > /tmp/.Install_LocOs/AddDataInfo.info

rm -f --force /tmp/.Install_LocOs/slimski.local.conf
rm -f --force /opt/AddDelUser/.TEMP/slimski.local.conf
cp --f --force /etc/slimski.local.conf /opt/AddDelUser/.TEMP/slimski.local.conf

cat /opt/AddDelUser/.live/etc/slimski.local.conf |sed 's/>_<-_-NEW_USER-_->_</'$NameUser'/g' |sed 's/>_<-_-AUTOLOGIN_USER-_->_</'$AUTOLOGIN'/g' > /tmp/.Install_LocOs/slimski.local.conf


if [[ $ret -eq 1 ]]; then
echo "LICENSEApp" > /tmp/.Install_LocOs/Button.info
exit -0
fi
if [[ $ret -eq 2 ]]; then
echo "AcceptsApp" > /tmp/.Install_LocOs/Button.info
cd /opt/AddDelUser/
sh Info_AddDelUser_Start.sh
fi
if [[ $ret -eq 3 ]]; then
echo "ExitApp" > /tmp/.Install_LocOs/Button.info
exit -0
fi



exit -0
 


