#! /bin/bash
clear

HERE="$(dirname "$(readlink -f "${0}")")"
PATH="${HERE}"/usr/bin/:"${HERE}"/usr/:"${HERE}"/lib/:"${PATH}"
LD_LIBRARY_PATH="${HERE}"/usr/lib64/:"${LD_LIBRARY_PATH}"

## export PERLLIB="${HERE}"/usr/share/perl5/:"${HERE}"/usr/lib/perl5/:"${PERLLIB}"

#who=`who` 
#Who="UserWho $who"  


# PosDir=`find $(pwd) -maxdepth 0 -type d -not -path '*/\.*' | sort`

DelLiveUser=$(cat /opt/AddDelUser/.live/UserLive.info)
NameUser=$(cat /tmp/.Install_LocOs/AddDataInfo.info | grep  "NameUser >" | awk '{ print $3}')

YAD="YadRunInfoAddDelUser"
Txt1="Eliminación de usuario:"
Txt2="Creación nuevo usuario:"
Txt3="en curso..."

AddDel_User()
{
echo "Install_LocOs"
cd /opt/AddDelUser/
sh AddDelUserLive.sh
sleep 3
sudo killall YadRunInfoAddDelUser
}

NameGif=$(cat /tmp/.Install_LocOs/NameGifAddDelUser.info)
FileName="${HERE}/usr/icons/$NameGif.gif"
AddDel_User | $YAD \
--picture --size=orig \
--text='<span font="22">'"$Txt1\n$DelLiveUser LIVE\n$Txt2:\n$NameUser\n$Txt3"'</span>' \
--filename=$FileName \
--auto-close --auto-kill --no-buttons --undecorated --center --skip-taskbar --borders="15" 

exit -0

